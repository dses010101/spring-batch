package org.des.configs;

import org.des.entities.EndUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileUrlResource;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Configuration
@EnableBatchProcessing
public class BatchConfig extends JobExecutionListenerSupport
{
    private static Logger log = LoggerFactory.getLogger(BatchConfig.class);

    // Job Creation ****************************************************************************************************

    @Autowired
	private JobBuilderFactory jobs;

    public Job createJob(final URL path)
    {
        return jobs
                .get(path.toString())
                .listener(this)
                .flow(step1(path))
                .end()
                .build();
    }

    // Batch configs ****************************************************************************************************

    @Autowired
    private StepBuilderFactory steps;

    @Qualifier("dataSource")
    @Autowired
    DataSource dataSource;

    protected Step step1(final URL path)
	{
		return steps.get("step1")
				.<EndUser, EndUser>chunk(50)
				.reader(itemReader(path))
				.writer(itemWriter())
				.build();
	}

	public ItemReader itemReader(final URL path)
	{
		FlatFileItemReader<EndUser> itemReader = new FlatFileItemReader<>();
		itemReader.setLinesToSkip(1); // It's assumed that we have a one header line.
        itemReader.setResource(new FileUrlResource(path));
		itemReader.setLineMapper(new DefaultLineMapper<EndUser>() {{
			setLineTokenizer(new DelimitedLineTokenizer()
			{{
				setNames("id", "name", "value");
			}});
			setFieldSetMapper(new BeanWrapperFieldSetMapper<EndUser>()
			{{
				setTargetType(EndUser.class);
			}});
		}});

		return itemReader;
	}

	@Bean
	public ItemWriter itemWriter()
	{
		JdbcBatchItemWriter<EndUser> itemWriter = new JdbcBatchItemWriter<>();
		itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
		itemWriter.setSql("INSERT INTO ENDUSER (id, name, value) VALUES (:id, :name, :value)");
		itemWriter.setDataSource(dataSource);

		return itemWriter;
	}

    // Jobs Initialization *********************************************************************************************

    @Value("${dataLoader.processingFolder}")
    private String processingFolder;
    @Value("${dataLoader.processedFolder}")
    private String processedFolder;

    @Autowired
    JobLauncher jobLauncher;

    @Bean
    void launchJobs()
    {
        final Path folder = Paths.get(processingFolder).normalize();
        try (final DirectoryStream<Path> csvs = Files.newDirectoryStream(folder, "*.csv"))
        {
            for (final Path csv : csvs)
            {
                Job job = createJob(csv.toUri().toURL());
                JobParameters jobParameters = new JobParametersBuilder().addString("path", csv.toString()).toJobParameters();
                jobLauncher.run(job, jobParameters);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred while exploring folder " + folder, e);
        }
    }

    // Moving processed csv-file *****************************************************************************************

    @Override
    public void beforeJob(JobExecution jobExecution)
    {
        final String path = jobExecution.getJobParameters().getString("path");
        log.info("Loading data from file: \"" + path + "\" is started.");
    }

    @Override
    public void afterJob(JobExecution jobExecution)
    {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED)
        {
            final String path = jobExecution.getJobParameters().getString("path");
            log.info("Loading data from file: \"" + path + "\" was finished.");
            moveCSV(Paths.get(path));
        }
    }

    private void moveCSV(final Path csv)
    {
        final Path target = Paths.get(processedFolder, csv.getName(csv.getNameCount() - 1).toString()).normalize();
        try
        {
            Files.move(csv, target, REPLACE_EXISTING);
            log.info("File: " + csv + " was moved to \"" + target + "\".");
        }
        catch (IOException e)
        {
            e.printStackTrace();
            log.error("Error occurred while moving file: " + csv, e);
        }
    }
}
