package org.des.entities;

import java.util.Objects;

public class EndUser
{
    private Integer id;

    private String name;

    private String value;

    //******************************************************************************************************************

    @SuppressWarnings("unused")
    public EndUser()
    {
    }

    @SuppressWarnings("unused")
    public EndUser(Integer id, String name, String value)
    {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    //******************************************************************************************************************

    @SuppressWarnings("unused")
    public Integer getId()
    {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(Integer id)
    {
        this.id = id;
    }

    @SuppressWarnings("unused")
    public String getName()
    {
        return name;
    }

    @SuppressWarnings("unused")
    public void setName(String name)
    {
        this.name = name;
    }

    @SuppressWarnings("unused")
    public String getValue()
    {
        return value;
    }

    @SuppressWarnings("unused")
    public void setValue(String value)
    {
        this.value = value;
    }

    //******************************************************************************************************************

    @Override
    public boolean equals(final Object otherObject)
    {
        if (this == otherObject)
            return true;

        if (otherObject == null || (getClass() != otherObject.getClass()))
            return false;

        final EndUser otherEntity = (EndUser) otherObject;

        return Objects.equals(id, otherEntity.id);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName() + ": " + String.valueOf(id);
    }
}