DROP TABLE IF EXISTS
  batch_job_execution,
  batch_job_execution_context,
  batch_job_execution_params,
  batch_job_instance,
  batch_step_execution,
  batch_step_execution_context,
  enduser
  CASCADE;

-- DROP SEQUENCE IF EXISTS
--   batch_job_execution_seq,
--   batch_job_seq,
--   batch_step_execution_seq
--   CASCADE;

CREATE TABLE batch_job_instance
(
  job_instance_id bigint                 NOT NULL,
  version         bigint,
  job_name        character varying(100) NOT NULL,
  job_key         character varying(32)  NOT NULL,
  CONSTRAINT batch_job_instance_pkey PRIMARY KEY (job_instance_id),
  CONSTRAINT job_inst_un UNIQUE (job_name, job_key)
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_job_instance
  OWNER TO postgres;

CREATE TABLE batch_job_execution
(
  job_execution_id           bigint                      NOT NULL,
  version                    bigint,
  job_instance_id            bigint                      NOT NULL,
  create_time                timestamp without time zone NOT NULL,
  start_time                 timestamp without time zone,
  end_time                   timestamp without time zone,
  status                     character varying(10),
  exit_code                  character varying(2500),
  exit_message               character varying(2500),
  last_updated               timestamp without time zone,
  job_configuration_location character varying(2500),
  CONSTRAINT batch_job_execution_pkey PRIMARY KEY (job_execution_id),
  CONSTRAINT job_inst_exec_fk FOREIGN KEY (job_instance_id)
    REFERENCES batch_job_instance (job_instance_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_job_execution
  OWNER TO postgres;

CREATE TABLE batch_job_execution_context
(
  job_execution_id   bigint                  NOT NULL,
  short_context      character varying(2500) NOT NULL,
  serialized_context text,
  CONSTRAINT batch_job_execution_context_pkey PRIMARY KEY (job_execution_id),
  CONSTRAINT job_exec_ctx_fk FOREIGN KEY (job_execution_id)
    REFERENCES batch_job_execution (job_execution_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_job_execution_context
  OWNER TO postgres;

CREATE TABLE batch_job_execution_params
(
  job_execution_id bigint                 NOT NULL,
  type_cd          character varying(6)   NOT NULL,
  key_name         character varying(100) NOT NULL,
  string_val       character varying(250),
  date_val         timestamp without time zone,
  long_val         bigint,
  double_val       double precision,
  identifying      character(1)           NOT NULL,
  CONSTRAINT job_exec_params_fk FOREIGN KEY (job_execution_id)
    REFERENCES batch_job_execution (job_execution_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_job_execution_params
  OWNER TO postgres;

CREATE TABLE batch_step_execution
(
  step_execution_id  bigint                      NOT NULL,
  version            bigint                      NOT NULL,
  step_name          character varying(100)      NOT NULL,
  job_execution_id   bigint                      NOT NULL,
  start_time         timestamp without time zone NOT NULL,
  end_time           timestamp without time zone,
  status             character varying(10),
  commit_count       bigint,
  read_count         bigint,
  filter_count       bigint,
  write_count        bigint,
  read_skip_count    bigint,
  write_skip_count   bigint,
  process_skip_count bigint,
  rollback_count     bigint,
  exit_code          character varying(2500),
  exit_message       character varying(2500),
  last_updated       timestamp without time zone,
  CONSTRAINT batch_step_execution_pkey PRIMARY KEY (step_execution_id),
  CONSTRAINT job_exec_step_fk FOREIGN KEY (job_execution_id)
    REFERENCES batch_job_execution (job_execution_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_step_execution
  OWNER TO postgres;

CREATE TABLE batch_step_execution_context
(
  step_execution_id  bigint                  NOT NULL,
  short_context      character varying(2500) NOT NULL,
  serialized_context text,
  CONSTRAINT batch_step_execution_context_pkey PRIMARY KEY (step_execution_id),
  CONSTRAINT step_exec_ctx_fk FOREIGN KEY (step_execution_id)
    REFERENCES batch_step_execution (step_execution_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE batch_step_execution_context
  OWNER TO postgres;

CREATE TABLE enduser
(
  id    integer              NOT NULL,
  name  character varying(5) NOT NULL,
  value character varying(5) NOT NULL,
  CONSTRAINT enduser_pkey PRIMARY KEY (id)
)
  WITH (
    OIDS= FALSE
  );
ALTER TABLE enduser
  OWNER TO postgres;